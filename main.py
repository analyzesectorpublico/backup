import os 
import requests
import json
from datetime import datetime
from pathlib import Path
from bs4 import BeautifulSoup as bs

from conf import email, token, password, solo_actualizaciones
print(datetime.now())

items = []
pageToken = None
while 1 == 1:
    if pageToken:
        url_lista = "https://compin.cerofilas.gob.cl/backend/api/tramites?token={}&maxResults=20&pageToken={}".format(token, pageToken)
    else:
        url_lista = "https://compin.cerofilas.gob.cl/backend/api/tramites?token={}&maxResults=20".format(token)
    resp = requests.get(url_lista)
    j = json.loads(resp.text)
    resultados = j["tramites"]["items"]
    pageToken = j["tramites"]["nextPageToken"]

    for resultado in resultados:
        items.append(resultado)

    if len(resultados) != 20:
        break

print("Se capturaron {} solicitudes".format(len(items)))


def login():
    session = requests.session()
    res = session.get("https://compin.cerofilas.gob.cl/backend/login")
    soup = bs(res.text)

    data = {
        '_token': soup.find("input").attrs["value"],
        'email': email,
        'password': password,
    }
    res = session.post("https://compin.cerofilas.gob.cl/backend/login", data=data)
    res = session.get("https://compin.cerofilas.gob.cl/backend")
    return session


session = login()
print("Ahora se guardarán los datos y se descargaran los archivos")

for item in items:
    print("Descargando solicitud {}".format(item["id"]))
    carpeta = "dump/{}".format(item["id"])
    Path(carpeta).mkdir(parents=True, exist_ok=True)
    archivo = "{}/data.json".format(carpeta)

    if solo_actualizaciones:
        if os.path.exists(archivo):
            f = open(archivo, "r")
            j = json.loads(f.read())
            if j["fecha_modificacion"] == item["fecha_modificacion"]:
                continue

    with open(archivo, 'w') as outfile:
        json.dump(item, outfile, indent=4)
        # pprint(item, stream=outfile)

    hrefs_leidos = []
    for etapa in item["etapas"]:
        carpeta_etapa = "dump/{}/{}".format(item["id"], etapa["id"])
        Path(carpeta_etapa).mkdir(parents=True, exist_ok=True)
        res = session.get("https://compin.cerofilas.gob.cl/backend/seguimiento/ver_etapa/{}".format(etapa["id"]))
        page = bs(res.text)
        for a in page.select("p.link>a"):
            href = a.attrs["href"]
            if len(href) < 10:
                continue
            if href in hrefs_leidos:
                continue
            hrefs_leidos.append(href)
            name_file = a.parent.parent.select_one("input").attrs["value"]
            tipo = a.parent.parent.select_one("input").attrs["name"]
            carpeta_etapa_archivo = "{}/{}".format(carpeta_etapa, tipo)
            Path(carpeta_etapa_archivo).mkdir(parents=True, exist_ok=True)
            archivo = "{}/{}".format(carpeta_etapa_archivo, name_file)
            res = requests.get(href, allow_redirects=True)
            open(archivo, 'wb').write(res.content)
